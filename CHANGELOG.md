## Change log

### 0.1.2
* Fixed nginx port redirect on /cheat
* Round timer seconds on win screen
* Parameterize boxes, text, and mouse regions on win/lose end game screens
* Improve check to see if game is won
* Added version to credits screen

### 0.1.1 
* Added nginx config and moved game to port 8080 
* Added cheat version to /cheat
* Fixed boxes, text, and mouse regions on start and option screen

### 0.1.0
* Initial version