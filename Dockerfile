FROM nginx:latest
COPY ./html /var/concentration/html
COPY ./code /var/concentration/html/code
COPY ./nginx.conf /etc/nginx/nginx.conf