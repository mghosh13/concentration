#!/bin/bash

set -o nounset

version="${1}"

docker build .  -t registry.gitlab.com/milang/concentration:latest
docker build .  -t "registry.gitlab.com/milang/concentration:${version}"

docker push registry.gitlab.com/milang/concentration:latest
docker push "registry.gitlab.com/milang/concentration:${version}"
